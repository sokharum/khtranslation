<?php 
 class MY_Controller extends BaseController
 {

 	public function __construct()
 	{
 		parent::__construct();
 		$GLOBALS['userData'] = array();
 		global $userData;
		$this->load->model('User_model');
 	}

 	public function index(){
 		$this->view();
 	}

 	public function view(){
 		global $userData;
		$controller = $userData['controller'];
		$this->rendertemplate("templates/default", "page/user/index", $data);
 	}
 } 
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Application_controller extends MY_Controller
{
	function __construct() {
		parent::__construct(true);
		global $user_data;
		$user_data['table'] = 'khApplications';
	}
}
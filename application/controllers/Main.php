<?php

class Main extends BaseController
{
    /*
     * 	Contructor load model and library in CI
     */

    function __construct()
    {
        parent::__construct(true);
        $this->load->model('User_model');
        $this->load->library('form_validation');
    }
    /*
     * Default home page
     */

    function index()
    {
        $this->rendertemplate("templates/default", "main/home");
    }

    function user()
    {
        $this->rendertemplate("templates/default", "page/user/index");
    }

    function login()
    {
        $this->rendertemplate("templates/login", "main/login");
    }
    /*
     * Checking user email, user password and token
     */

    function validate_login()
    {
        $this->form_validation->set_rules('user_email', 'Email', 'required|min_length[8]|max_length[55]');
        $this->form_validation->set_rules('user_pwd', 'Password', 'required|min_length[8]|max_length[16]');
        if ($this->form_validation->run() == FALSE) :
            $this->login();
        else:
            $params = $this->getParameter();
            $params["ip_address"] = $this->input->ip_address();
            if (!isset($params["ip_address"]) || is_null_or_empty($params["ip_address"]) ||
                !isset($params["user_email"]) || is_null_or_empty($params["user_email"]) ||
                !isset($params["user_pwd"]) || is_null_or_empty($params["user_pwd"])) :
                echo "There is error with the data";
                return;
            endif;

            $user_login = $this->User_model->check_existing_email($params);
            if ($user_login == null) :
                echo "Email is incorrect";
                exit;
                return;
            endif;

            if (!BaseEncryption::password_verify($params["user_pwd"], $user_login["user_pwd"])) :
                echo "Password is incorrect";
                exit;
                return;
            endif;
            $params["user_id"] = $user_login["user_id"];
            $params["login_token"] = generate_token();
            $this->User_model->create_user_signin($params);
            $session_data = array(
                'user_email' => $user_login['user_email'],
                'login_token' => $params['login_token']
            );
            $this->setSessionUser($session_data);
            redirect(site_url('main'));
        endif;
    }
    /*  Submit {Validation} |>> request_register {Set value to input fields} |>> Retrive email from request |>> verify code
      Opening Check validation in Frontend web browser
      register is result return false error during submit data from register.
      request_register is result return true getting data checking for user request and update is email have existing in table kh_user_request
     */

    public function validate_form()
    {
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|callback_isValidate');
        $this->form_validation->set_rules('user_email', 'Email', 'required|callback_isValidate|max_length[55]');
        $this->form_validation->set_rules('user_pwd', 'Password', 'required|min_length[8]|max_length[16]');

        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[user_pwd]');

        if ($this->form_validation->run() == false) :
            $this->register();
        else :
            $parameter = $this->getParameter();
            $check_existing_email = $this->User_model->check_existing_email($parameter);
            if ($check_existing_email == null) :
                $this->request_register($parameter);
            else:
                $sms_error = "<div class='alert alert-error error'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
				Email is invalid or already token
				</div>";
                $this->setFlashData("error_message", $sms_error);
                redirect(site_url("main/register"));
            endif;
        endif;
    }

    // 	Set or Make input as empty #There were problems to creating your account.
    function register()
    {
        $data = array();
        $register_data = array(
            "full_name" => "",
            "user_email" => "",
            "user_pwd" => "",
            "confirm_password" => ""
        );
        //var_dump($this->session->all_userdata());
        $flash_data = $this->getFlashData("error_message");
        if ($flash_data != null) {
            $data["error"] = $flash_data;

            $register_flash = $this->getFlashData("register_data");
            if ($register_flash != null) {
                $register_data = array_merge($register_flash);
            }
        }
        $data["register_data"] = $register_data;
        $this->rendertemplate("templates/login", "main/register", $data);
    }
    /*
     * 
     * Action From Form input Do Validate Backend, and do sending email
     */

    function request_register($parameter)
    {
        if ($parameter["user_email"] == "") {
            $this->sms(0, 'Email is invalid or already token');
            unset($parameter["user_pwd"]);
            unset($parameter["confirm_password"]);
            $this->setFlashData("register_data", $parameter);
            redirect(site_url("main/register"));
        } else {
            $parameter["req_token"] = generate_token();
            $parameter["req_code"] = generateRandomNumber();
            $signup = $this->User_model->check_signup_by_email($parameter);
            if ($signup == null) :
                $this->User_model->create_request_signup($parameter);
            else :
                $this->User_model->update_request_signup($parameter);
            endif;
            $return = execInBackground($this->php_exe_path . " -f " . FCPATH . "/index.php jobs/sendsignupconfirm/" . $parameter["req_token"]);
            if ($return != NULL):
                $this->rendertemplate("templates/login", "main/register");
            else :
                $this->rendertemplate("templates/login", "main/verify_code");
            endif;
        }
    }
    /*
     * 	to do is validatioin for submit code 6 digit
     * 	1, validate input
     * 	2, checking verify user input code with the code store in db.
     * 	3, create user with specific field
     * 	4, delete request sign up to prevent inject from injector.
     */

    /*
     * 	1, The cofirm_code was call from submit confirm code UI
     */

    function confirm_code()
    {
        $this->form_validation->set_rules('req_code', 'Verify Code', 'required|numeric|min_length[6]|max_length[6]');
        if ($this->form_validation->run() == FALSE):
            $this->rendertemplate("templates/login", "main/verify_code");
        else:
            $signup = $this->User_model->verify_signup_code($this->getParameter());
            if ($signup == null) :
                $this->sms(0, 'Verification code is not correct');
                $data = array();
                $register_data = array(
                    "full_name" => "",
                    "user_email" => "",
                    "user_pwd" => "",
                    "confirm_password" => ""
                );
                //var_dump($this->session->all_userdata());
                $flash_data = $this->getFlashData("error_message");
                if ($flash_data != null) {
                    $data["error"] = $flash_data;

                    $register_flash = $this->getFlashData("register_data");
                    if ($register_flash != null) {
                        $register_data = array_merge($register_flash);
                    }
                }
                $data["register_data"] = $register_data;
                $this->rendertemplate("templates/login", "main/verify_code", $data);
            else:
                $this->User_model->user_create($signup);
                $this->confirm_action();
                $this->User_model->delete_request_signup($signup);
            endif;
        endif;
    }
    /*
     * 	showing alert to confirm code 6 length
     * 	do redirect to log in page if the user input value match with DB
     */

    function confirm_action()
    {
        $url = site_url('main/login');
        echo "<script>
                    function alert_sms() {
                        setTimeout(function() {
                            swal({
                                title: 'Excellent!',
                                text: 'Verify code success!',
                                type: 'success'
                            }, function() {
                                window.location = '" . $url . "';
                            });
                        }, 1000);
                    }
                    alert_sms.call();
                    </script>";
        $this->rendertemplate("templates/login", "main/verify_code");
    }

    function signout()
    {
        $this->clearSessionUser();
        redirect(site_url('main/login'));
    }
    /*
     * 	Showing message related to action {Error or Success}
     */

    function sms($type, $sms)
    {
        $sms_error = "<div class='alert alert-error error'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
		" . $sms . ".
		</div>";
        $sms_success = "<div class='alert alert-success success'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
		" . $sms . ".
		</div>";
        if ($type == 0):
            $this->setFlashData("error_message", $sms_error);
        else:
            $this->setFlashData("success_message", $sms_success);
        endif;
    }

    // Call Back Form validation Script
    function isValidate($field)
    {
        if (!preg_match('/[^@ a-z A-Z 0-9]/', $field)) {
            $this->form_validation->set_message('isValidate', "%s can't be blank!");
            return True;
        }
    }
}

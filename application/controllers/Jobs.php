<?php
class Jobs extends BaseController {
	function __construct() {
		parent::__construct(false);
		$this->load->model("User_model");
	}
	
	function sendsignupconfirm($token) {
		$signup = $this->User_model->check_signup_by_token(array("req_token" => $token));
		if ($signup == null) :
			//TODO could not found signup
			return;
		endif;
		
		$body = "Hello,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= $signup["req_code"]." is your Khmer Translation verification code";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "Thank you,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "khawin.com";
		
		$this->load->library('email', $this->email_config);
		$this->email->set_newline(BaseConstant::EMAIL_LINE_SEPARATOR);
		$this->email->from("no_reply@getloi.com");
		$this->email->to($signup["user_email"]);
		$this->email->subject("Khawin confirmation code");
		$this->email->message($body);
		
		if ($this->email->send()) {
			//TODO send successfully
			echo "success";
		} else {
			echo $this->email->print_debugger();
			//TODO log error
		}
	}
	/*
	function sendresetpwdconfirm($reset_token) {
		$reset_data = $this->muser->check_reset_data(array("reset_token" => $reset_token));
		if ($reset_data == null) :
			echo "1";
			return;
		endif;
		
		$user = $this->muser->user_by_id($reset_data["user_id"]);
		if ($user == null) :
			echo "2";
			return;
		endif;
		
		$body = "Hello,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "Your reset password verification code is: ".$reset_data["reset_code"];
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "Thank you,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "khawin.com";
		
		$this->load->library('email', $this->email_config);
		$this->email->set_newline(BaseConstant::EMAIL_LINE_SEPARATOR);
		$this->email->from("no_reply@getloi.com");
		$this->email->to($user["user_email"]);
		$this->email->subject("Khawin reset password");
		$this->email->message($body);
			
		if ($this->email->send()) {
			//TODO send successfully
			echo "success";
		} else {
			//echo $this->email->print_debugger();
			//TODO log error
		}
	}
	
	function sendnewpwd($reset_token) {
		$this->load->model("muser");
		$reset_pwd = $this->muser->check_reset_data(array("reset_token" => $reset_token));
		if ($reset_pwd == null) :
			return;
		endif;
		
		$this->muser->update_resetpwd_done(array("reset_token" => $reset_token));
		$new_pwd = generateRandomString();
		$this->muser->reset_password(array("user_id" => $reset_pwd["user_id"], "user_pwd" => $new_pwd));
		
		$user = $this->muser->user_by_id($reset_pwd["user_id"]);
		
		$body = "Hello,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "Your new password is: ".$new_pwd;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "Thank you,";
		$body .= BaseConstant::EMAIL_LINE_SEPARATOR;
		$body .= "khawin.com";
		
		$this->load->library('email', $this->email_config);
		$this->email->set_newline(BaseConstant::EMAIL_LINE_SEPARATOR);
		$this->email->from("no_reply@getloi.com");
		$this->email->to($user["user_email"]);
		$this->email->subject("Khawin reset password");
		$this->email->message($body);
			
		if ($this->email->send()) {
			//TODO send successfully
			echo "success";
		} else {
			//echo $this->email->print_debugger();
			TODO log error_log(message)
		}
		
	}*/
}
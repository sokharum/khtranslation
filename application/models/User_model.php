<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends BaseModel
{

    public function __construct()
    {
        parent::__construct();
    }

    // Checking for email existing in DB
    function check_signup_by_email($data)
    {
        $this->db->where("user_email", $data["user_email"]);
        $query = $this->db->get("kh_signup_request");
        if ($query->num_rows() <= 0) :
            return null;
        else :
            return $query->row_array();
        endif;
    }

    // checking token from UI with DB don't not used
    function check_signup_by_token($data)
    {
        $this->db->where("req_token", $data["req_token"]);
        $query = $this->db->get("kh_signup_request");
        if ($query->num_rows() <= 0) :
            return null;
        else :
            return $query->row_array();
        endif;
    }

    //	create request sign up request
    function create_request_signup($data)
    {
        $this->db->set("req_token", $data["req_token"]);
        $this->db->set("req_date", $this->current_timestamp(), false);
        $this->db->set("req_date_expire", $this->current_timestamp_plus_minute(10), false);
        $this->db->set("user_email", $data["user_email"]);
        $this->db->set("user_pwd", BaseEncryption::password_encrypt($data["user_pwd"]));
        $this->db->set("req_code", $data["req_code"]);
        $this->db->insert("kh_signup_request");
    }

    //	update request sign upu
    function update_request_signup($data)
    {
        $this->db->set("req_token", $data["req_token"]);
        $this->db->set("req_date", $this->current_timestamp(), false);
        $this->db->set("req_date_expire", $this->current_timestamp_plus_minute(10), false);
        $this->db->where("user_email", $data["user_email"]);
        $this->db->set("user_pwd", BaseEncryption::password_encrypt($data["user_pwd"]));
        $this->db->set("req_code", $data["req_code"]);
        $this->db->update("kh_signup_request");
    }

    //	Verify code 6 lang with DB

    function verify_signup_code($data)
    {
        $this->db->where("req_code", $data["req_code"]);
        $this->db->where("req_date_expire >= ", $this->current_timestamp(), false);
        $query = $this->db->get("kh_signup_request");
        if ($query->num_rows() <= 0) :
            return null;
        else :
            return $query->row_array();
        endif;
    }
    /*
     * 	For sing up request;
     */

    function check_existing_email($data)
    {
        $this->db->where("user_email", $data["user_email"]);
        $query = $this->db->get("kh_user");
        if ($query->num_rows() <= 0) :
            return null;
        else :
            return $query->row_array();
        endif;
    }

    // 	create user for table kh_user
    function user_create($data)
    {
        $this->db->set("user_email", $data["user_email"]);
        $this->db->set("user_pwd", $data["user_pwd"]);
        $this->db->set("date_create", $this->current_timestamp(), false);
        $this->db->insert("kh_user");
    }

    //	Delete request from table kh_signup_request when create user success
    function delete_request_signup($data)
    {
        $this->db->where("user_email", $data["user_email"]);
        $this->db->delete("kh_signup_request");
    }

    function create_user_signin($data)
    {
        $this->db->set("login_token", $data["login_token"]);
        $this->db->set("user_id", $data["user_id"]);
        $this->db->set("ip_address", $data["ip_address"]);
        $this->db->set("date_login", $this->current_timestamp(), false);
        $this->db->set("expire_date", $this->current_timestamp_plus_minute(10), false);
        $this->db->set("date_access", $this->current_timestamp(), false);
        $this->db->set("login_active", 1);
        $this->db->insert("kh_user_login_token");
    }

    function update_user_signin($data)
    {
        $this->db->where("login_token", $data["login_token"]);
        $this->db->set("ip_address", $data["ip_address"]);
        $this->db->set("date_login", $this->current_timestamp(), false);
        $this->db->set("expire_date", $this->current_timestamp_plus_minute(10), false);
        $this->db->set("date_access", $this->current_timestamp(), false);
        $this->db->set("login_active", 1);
        $this->db->update("kh_user_login_token");
    }
}

?>
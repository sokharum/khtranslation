<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" style="position:fixed; top: 0px;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       <li>
          <a href="#">
            <span>NEOFI Exchange Hub</span>
          </a>
        </li>
        <li class="header">MAIN MENU</li>
        <li class="active">
          <a href="<?=base_url('user');?>">
            <i class="fa fa-users"></i> <span>Users</span>
          </a>
        </li>
        <li>
          <a href="<?=base_url('app');?>">
            <i class="fa fa-users"></i> <span>Apps</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-file"></i> <span>Labels</span>
            
          </a>
        </li>
         <li>
          <a href="#">
            <i class="fa fa-object-ungroup"></i> <span>Groups</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-language"></i> <span>Languages</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
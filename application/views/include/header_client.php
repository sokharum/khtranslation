<div class="container">
<nav id="mainNav" class="navbar navbar-custom navbar-default navbar-fixed-top">
<div class="container">
	<div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_manu">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=site_url()?>"><img class="app_logo" src="<?=base_url("img/logo_word.png")?>"></a>
    </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="main_manu">
	     <ul class="nav navbar-nav">
			 <li><a href="<?=site_url()?>">Home</a></li>
			 <li><a href="<?=site_url("site/career")?>">Career</a></li>
			 <li><a href="<?=site_url("site/aboutus")?>">About us</a></li>
			 <li><a href="<?=site_url("site/contactus")?>">Contact us</a></li>
	 	 </ul>		
   </div>
</div>
</nav>
  
</div>
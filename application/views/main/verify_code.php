<div class="login-box">
	<div class="login-logo">
        <a href="<?= site_url(); ?>">KH Translation</a>
	</div>
	
	<?php if (isset($error))  : ?>
            <?=$error?>
    <?php endif;?>

	<div class="login-box-body">
		<h1 class="login-project" style="text-align: center;">KH Translation</h1>
		<p class="login-box-msg">Verify Code</p>
		<p>Please confirm your code that we was sent by E-mail.</p>

		<form action="<?= site_url("confirm_code") ?>" method="post">

			<div class="form-group has-feedback">
				<input class="form-control" type="text" name="req_code" placeholder="Enter the 6 digit code" value="<?= set_value('req_code') ?>">
                <span class="error"><?php echo form_error('req_code'); ?></span>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Verify</button>
				</div>
			</div>
		</form>
		<br>
		<a href="http://khawin.com/"><p style="text-align: center;">www.khawin.com<p></a>
	</div>
</div>
<div class="register-box">
    <div class="register-logo">
        <a href="<?= site_url(); ?>">KH Translation</a>
    </div>
    <?php if (isset($error)) : ?>
        <?= $error ?>
    <?php endif; ?>

    <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>
        <form action="<?= site_url('validate_form'); ?>" method="post">
            <div class="form-group has-feedback">
                <input name="full_name" type="text" class="form-control" placeholder="Full name" value="<?= (set_value('full_name') == NULL ? htmlspecialchars($register_data["full_name"]) : set_value('full_name')); ?>" >
                <span class="glyphicon ghtmlspecialcharslyphicon-user form-control-feedback"></span>
                <span class="error"><?= form_error('full_name'); ?></span>
            </div>
            <div class="form-group has-feedback">
                <input id="user_email" name="user_email" type="email" class="form-control" placeholder="Email" value="<?= (set_value('user_email') == NULL ? htmlspecialchars($register_data["user_email"]) : set_value('user_email')); ?>">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span class="error"><?= form_error('user_email'); ?></span>
            </div>
            <div class="form-group has-feedback">
                <input name="user_pwd" type="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span class="error"><?= form_error('user_pwd'); ?></span>
            </div>
            <div class="form-group has-feedback">
                <input  name="confirm_password" type="password" class="form-control" placeholder="Retype password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                <span class="error"><?= form_error('confirm_password'); ?></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <!-- <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
            Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flaregister_datat"><i class="fa fa-google-plus"></i> Sign up using
            Google+</a>
        </div> -->

        <a href="<?= site_url("main/login") ?>" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
</div>
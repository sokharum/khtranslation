<?php
/**
 * Custom base controler
 * @author sokha
 *
 */
class BaseController extends CI_Controller {
	protected  $php_exe_path = "php";
	
	protected $email_config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'just136.justhost.com',
		'smtp_port' => 26,
		'smtp_user' => 'no-reply@khawin.com',
		'smtp_pass' => '@Khawin123',
		'mailtype' => 'text',
		'charset' => 'utf-8',
		'wordwrap' => TRUE
	);
	
	/**
	 * Constructor of this controller
	 */
	function __construct($control_session=true) {
		parent::__construct();
		$this->initController($control_session);
	}
	
	/**
	 * Rendering view
	 * @param unknown $template template name
	 * @param unknown $view the view to render
	 * @param unknown $data the data
	 */
	protected function rendertemplate($template, $view, $data=array()) {
		$data["app_main_view"] = $view;
		$this->load->view($template, $data);
	}
	
	/**
	 * Initialize the controller
	 * @param string $control_session
	 */
	private function initController($control_session=true) {
		$this->load->helper("url");
		$this->load->helper("base");
		require_once APPPATH.'third_party/BaseConstant.php';
		require_once APPPATH.'third_party/BaseEncryption.php';
		require_once BASEPATH.'core/Model.php';
		require_once APPPATH.'third_party/BaseModel.php';
		//require_once APPPATH.'third_party/ResultAPI.php';
		$this->load->library('session');
		if ($control_session) :
			$this->controlSession();
		endif;
	}
	
	protected function getSessionUser() {
		return $this->session->userdata(BaseConstant::SESSION_USER);
	}
	
	protected function setSessionUser($user) {
		$this->session->set_userdata(BaseConstant::SESSION_USER, $user);
	}

	protected function getSessionData($data_key) {
		return $this->session->userdata($data_key);	
	}
	
	protected function setSessionData($data_key, $data) {
		$this->session->set_userdata($data_key, $data);
	}

	protected function getFlashData($data_key) {
		return $this->session->flashdata($data_key);	
	}
	
	protected function setFlashData($data_key, $data) {
		$this->session->set_flashdata($data_key, $data);
	}
	protected function clearSessionUser(){
		$this->session->sess_destroy();
	}
	/**
	 * Controler user
	 * @param  int $session_status
	 */
	private function controlSession() {
		$user = $this->getSessionUser();
		$uri = trim(uri_string(), "/");
		return;
		if ($user != null) :
			if ($uri == "" || $uri == "main/login" || $uri == "main/validate_login" || $uri == "main/confirm_code" || $uri == "main/request_signup" || $uri == "main/register" || $uri == "main/validate_form") :
				redirect(site_url("main"));
				return;			
			endif;
		elseif ($uri == "main/login" || $uri == "main/validate_login" || $uri == "main/register" || $uri == "main/request_signup" ) :
				    // Do nothing
		elseif ($uri == "main/validate_login" || $uri == "main/validate_login" ) :
				if($req_data == null) :
				redirect(site_url("main/register"));				
				return;			
			endif;
		else:
				redirect(site_url("main/login"));
				return;	
		endif;
		}

		protected function getParameter() {
		//header("Content-Type: application/json; charset=UTF-8");
		//header('Access-Control-Allow-Origin: *');
		//header('Access-Control-Allow-Methods: GET, POST');

		//return json_decode(file_get_contents('php://input'),true);
			return $_POST;
		}

		protected function checkLoginToken($params, &$member, &$result) {
			if (!isset($params["login_token"]) || is_null_or_empty($params["login_token"])) :
				$result->error_code = ResultAPI::ERROR_DATA_TOKEN;
				$result->error_message = "There is error with the data token";
				echo $result->to_json();
				exit;
			endif;

			$this->load->model("member_m");
			$member = $this->member_m->check_signin_token($params);

			if ($member == null) :
				$result->error_code = ResultAPI::ERROR_DATA_TOKEN;
				$result->error_message = "There is error with the data token";
				echo $result->to_json();
				exit;
			endif;
		}

		protected function requestAPI($api_method, $params = array()) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, BaseController::DATA_COLLECTION_API_URL.$api_method);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

		//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

			$response = curl_exec($ch);
			curl_close($ch);
			return json_decode($response);
		}
	}
<?php
class BaseConstant {
	const APP_NAME = "KH Translation";
	const SALT = "AZERTYQWERTY";
	const KEY_ENCRYPT_SESSION = "KHAWIN_TRANSLATION";
	
	const SESSION_USER = "KH_SESSION_USER";
	const EMAIL_LINE_SEPARATOR = "\r\n";
}
<?php
/**
 * Encryption
 *
 */
class BaseEncryption {
	const ENCRYPTION_TIME = 2;
	/**
	 * Encode a string
	 * @param String $str
	 */
	static private function k_encode($str) {
	  for($i=0; $i<BaseEncryption::ENCRYPTION_TIME; $i++) {
	    $str=strrev(base64_encode($str)); //apply base64 first and then reverse the string
	  }
	  return $str;
	}
	
	/**
	 * Decode a string
	 * @param String $str
	 */
	static private function k_decode($str) {
	  for($i=0; $i<BaseEncryption::ENCRYPTION_TIME; $i++) {
	    $str=base64_decode(strrev($str)); //apply base64 first and then reverse the string}
	  }
	  return $str;
	}
	
	/**
	 * String to Hexadecimal
	 * @param String $string
	 */
	static private function k_strhex($string) {
	    $hex="";
	    for ($i=0;$i<strlen($string);$i++) {
	        $hex.=(strlen(dechex(ord($string[$i])))<2)? "0".dechex(ord($string[$i])): dechex(ord($string[$i]));
	    }
	    return $hex;
	}

	/**
	 * Hexadecimal to String
	 * @param String $hex
	 */
	static private function k_hexstr($hex) {
	    $string="";
	    for ($i=0;$i<strlen($hex)-1;$i+=2) {
	        $string.=chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}

	/**
	 * Encrypt a String
	 * @param String $str
	 */
	static function encrypt($str) {
	    return BaseEncryption::k_strhex(OPEncryption::k_encode($str));
	} 

	/**
	 * Decrypt a string
	 * @param String $str
	 */
	static function decrypt($str) {
	   return BaseEncryption::k_decode(OPEncryption::k_hexstr($str));
	}
	
	static function encrypt_array($array_input) {
		return BaseEncryption::encrypt(serialize($array_input));
	}
	
	static function decrypt_array($enc_array) {
		return unserialize(BaseEncryption::decrypt($enc_array));
	}

	static function password_encrypt($password) {
		return password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
	}
	
	static function password_verify($password, $hash) {
		return password_verify($password, $hash);
	}
}
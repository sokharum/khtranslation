<?php
/**
 * BaseModel class
 * @author sokha
 */
class BaseModel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	/**
	* loading another model
	* @param $model_name the model name to load
	* @author sokha
	*/
	function load_other_model($model_name) {
		$CI =& get_instance();
		$CI->load->model($model_name);
		return $CI->$model_name;
	}
	
	/**
	 * getting the next sequence id
	 * @param $seq_name the name of sequence
	 * @author sokha
	 */
	
	function get_next_id($seq_name) {
		$sql = "SELECT nextval('".$seq_name."') AS next_id";
		$query = $this->db->query($sql);
		$nb_rows = $query->num_rows();
		if ($nb_rows < 1) {
			return NULL;
		} else {
			$row = $query->row_array();
			return $row["next_id"];
		}
	}
	
	function current_timestamp() {
		switch ($this->config->item('dbdriver')) :
		case "postgre":
			$result = "CURRENT_TIMESTAMP";
		break;
		case "mysql":
			$result = "CURRENT_TIMESTAMP";
			break;
		default:
			$result = "CURRENT_TIMESTAMP";
			break;
			endswitch;
			return $result;
	}
	
	function current_timestamp_plus_minute($minutes) {
	
		switch ($this->config->item('dbdriver')) :
		case "postgre":
			$result = "CURRENT_TIMESTAMP + (10 * interval '".$minutes." minute')";
		break;
		case "mysql":
			$result = "CURRENT_TIMESTAMP + INTERVAL ".$minutes." MINUTE";
			break;
		default:
			$result = "CURRENT_TIMESTAMP + (10 * interval '".$minutes." minute')";
			break;
			endswitch;
			return $result;
	}
}